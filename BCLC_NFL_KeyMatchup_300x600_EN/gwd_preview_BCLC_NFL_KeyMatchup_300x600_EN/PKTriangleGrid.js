;( function( window ) {

  'use strict';
    
  /**
   * Extend obj function
   *
   * This is an object extender function. It allows us to extend an object
   * by passing in additional variables and overwriting the defaults.
   */
  function extend( a, b ) {
    for( var key in b ) { 
      if( b.hasOwnProperty( key ) ) {
        a[key] = b[key];
      }
    }
    return a;
  }
    

  /**
   * PKTriangleGrid
   */
  function PKTriangleGrid( options ) {
    // function body...
     this.options = extend( {}, this.options );
     extend( this.options, options ); 
     this._init(); 
  }
    
   /**
   * PKTriangleGrid options Object
   *
   * @type {HTMLElement} wrapper - The wrapper to create the ray in.
   */
  PKTriangleGrid.prototype.options = {
    canvas : document.body,
    twidth: 100,
    theight : 100,
    tx : 0,
    ty: 0
  }

  PKTriangleGrid.prototype._init = function() {
      this.tick = 0;
      // create element
      this.ctx = this.options.canvas.getContext("2d");
      this.twidth = this.options.twidth;
      this.theight = this.options.theight;
      this.tx = this.options.tx;
      this.ty = this.options.ty;
      this.cwidth = this.options.canvas.scrollWidth;
      this.cheight = this.options.canvas.scrollHeight;
      
      console.log('init grid',this);
      
      var maxcol = Math.ceil( this.cwidth / (this.twidth/2) );
      var maxrow = Math.ceil( this.cheight / this.theight );
      
      this.arr = new Array( maxcol+1 );
      for (var i=0;i<=maxcol;i++){
          this.arr[i] = new Array( maxrow+1 );
      }
      
      this.update();
      this.start();
    };
    
    PKTriangleGrid.prototype.drawGrid = function() {
      
      this.ctx.clearRect(0,0,this.cwidth,this.cheight);
      
        
      
      var maxcol = Math.ceil( this.cwidth / (this.twidth/2) );
      var maxrow = Math.ceil( this.cheight / this.theight );
        
      
        
      for (var i=-1; i<=maxcol; i++){
          var sx = this.tx + i*(this.twidth/2);
          for (var j=Math.floor(maxrow/2);j<=maxrow; j++){
              var sy = this.ty + j*this.theight;
              var isUp = (j%2==0 && i%2==0) || (j%2==1 && i%2==1);
              var a = this.checkGrid( i, j );
              if (a==0) continue;
              
              this.ctx.beginPath();
              if (isUp){
                  this.ctx.moveTo(sx+this.twidth/2, sy);
                  this.ctx.lineTo(sx+this.twidth, sy+this.theight);
                  this.ctx.lineTo(sx, sy+this.theight);
              } else {
                  this.ctx.moveTo(sx, sy);
                  this.ctx.lineTo(sx+this.twidth, sy);
                  this.ctx.lineTo(sx+this.twidth/2, sy+this.theight);
              }
              this.ctx.closePath();
              
              a *= .7;

              this.ctx.fillStyle = "rgba(255, 255, 255, "+a+")";
              this.ctx.fill();
          }
      }
      
        
      
        
    }
    
    PKTriangleGrid.prototype.checkGrid = function( x, y ){
        var oval = this.arr[x+1][y+1] || 0;
        
        var isGoingUp = oval > 0;
        var shouldSwitchDirections = Math.random()>.95;
        
        var tval;
        if (isGoingUp){
            if (shouldSwitchDirections){
                oval *= -1;
                tval = 0
            } else {
                tval = 1;
            }
        } else {
            if (shouldSwitchDirections){
                oval *= -1;
                tval = 1;
            } else {
                tval = 0;
            }
        }
        
        var nval = oval - (oval-tval)*.2;
        this.arr[x+1][y+1] = nval;
        
        return Math.abs( nval );
    }
                                      
    PKTriangleGrid.prototype.update = function() {
        this.tick++;
        if (this.tick%3==0){
            this.drawGrid();
        }
    }
    
    PKTriangleGrid.prototype.start = function(){
        //console.log('start',this);
        TweenLite.ticker.addEventListener( 'tick', this.update, this );
    }
    PKTriangleGrid.prototype.stop = function(){
        //console.log('stop',this);
        TweenLite.ticker.removeEventListener( 'tick', this.update );
    }
    
    
    
  /**
   * Add to global namespace
   */
  window.PKTriangleGrid = PKTriangleGrid;

})( window );