;( function( window ) {

  'use strict';
    
  /**
   * Extend obj function
   *
   * This is an object extender function. It allows us to extend an object
   * by passing in additional variables and overwriting the defaults.
   */
  function extend( a, b ) {
    for( var key in b ) { 
      if( b.hasOwnProperty( key ) ) {
        a[key] = b[key];
      }
    }
    return a;
  }
    

  /**
   * PKTriangleMasker
   */
  function PKTriangleMasker( options ) {
    // function body...
     this.options = extend( {}, this.options );
     extend( this.options, options ); 
     this._init(); 
  }
    
   /**
   * PKTriangleMasker options Object
   *
   * @type {HTMLElement} wrapper - The wrapper to create the ray in.
   */
  PKTriangleMasker.prototype.options = {
    canvas: document.body,
    fill: document.body,
    twidth: 100,
    theight : 100,
    tx : 0,
    ty: 0
  }

  PKTriangleMasker.prototype._init = function() {
      this.tick = 0;
      // create element
      this.ctx = this.options.canvas.getContext("2d");
      this.fill = this.options.fill;
      this.twidth = this.options.twidth;
      this.theight = this.options.theight;
      this.tx = this.options.tx;
      this.ty = this.options.ty;
      this.cwidth = this.options.canvas.scrollWidth;
      this.cheight = this.options.canvas.scrollHeight;
      
      console.log('init trimask',this);
      
      this.masklevel = 1;
      this.maskrange = .5;
      
      this.update();
      this.start();
    };
    
    PKTriangleMasker.prototype.drawGrid = function() {
      this.ctx.clearRect(0,0,this.cwidth,this.cheight);
        
      this.ctx.globalCompositeOperation = "source-over";
        
      this.ctx.beginPath();
      
      var maxcol = Math.ceil( this.cwidth / (this.twidth/2) );
      var maxrow = Math.ceil( this.cheight / this.theight );
        
      var start = Math.round( this.masklevel * maxcol );
      for (var i=start-1; i<=maxcol; i++){
          var sx = this.tx + i*(this.twidth/2);
          for (var j=0;j<=maxrow; j++){
              var sy = this.ty + j*this.theight;
              var isUp = (j%2==0 && i%2==0) || (j%2==1 && i%2==1);
              if (Math.random()>this.maskrange) continue;
              if (isUp){
                  this.ctx.moveTo(sx+this.twidth/2, sy);
                  this.ctx.lineTo(sx+this.twidth, sy+this.theight);
                  this.ctx.lineTo(sx, sy+this.theight);
              } else {
                  this.ctx.moveTo(sx, sy);
                  this.ctx.lineTo(sx+this.twidth, sy);
                  this.ctx.lineTo(sx+this.twidth/2, sy+this.theight);
              }
          }
      }
      this.ctx.closePath();
        
      this.ctx.fillStyle = "#FFFFFF";
      this.ctx.fill();
    
      this.ctx.globalCompositeOperation = "source-atop";
      this.ctx.drawImage(this.fill,0,0);
    }
                                      
    PKTriangleMasker.prototype.update = function() {
        this.tick++;
        if (this.tick%8==0){
            this.drawGrid();
        }
    }
    
    PKTriangleMasker.prototype.start = function(){
        //console.log('start',this);
        TweenLite.ticker.addEventListener( 'tick', this.update, this );
    }
    PKTriangleMasker.prototype.stop = function(){
        //console.log('stop',this);
        TweenLite.ticker.removeEventListener( 'tick', this.update );
    }
    
    
    
  /**
   * Add to global namespace
   */
  window.PKTriangleMasker = PKTriangleMasker;

})( window );